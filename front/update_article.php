<?php 
include_once("../include/header.php");
include('../function/article_function.php');
include('../function/categorie_function.php');
include('../function/commentaire_function.php');
include('../function/utilisateur_function.php');

if (!isset($_SESSION['user']) || $_SESSION['user']->role == 3) {
    header('Location:../front/index.php');
} else {

if(isset($_GET['article_id'])){
    $article = GetArticleById($_GET['article_id']);
    $commentaires = GetAllCommentaireForOneArticle($_GET['article_id']);
}

?>
<div>
    <h1>Mise a jour de l'article : </h1>
    <form action="../back/update_article.php" method="post">
        <div>
            <input type="hidden" name="id_article" value="<?php echo($_GET['article_id']) ?>"> 
            <div>
                <label class="form-label" for="titre">Titre  :</label>
                <input class="form-control" type="titre" name="titre" id="titre" value="<?php echo($article->titre) ?>" required />
            </div>
            <div>
                <label class="form-label" for="contenu">Contenu :</label>
                <input class="form-control" type="contenu" name="contenu" id="contenu" value="<?php echo($article->contenu) ?>" required />
            </div>
            <div>
                <label class="form-label" for="image">Image :</label>
                <input class="form-control" type="text" name="image" id="image" value="<?php echo($article->image) ?>" required />
            </div>
            <input class="btn btn-primary" type="submit" value="Modifier" />
        </div>
    </form>
</div>


<?php
}
include_once("../include/footer.php");
?>
