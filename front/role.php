<?php
include_once("../include/header.php");
include('../model/role.php');   
include('../function/role_function.php');
include('../function/utilisateur_function.php');

if (!isset($_SESSION['user']) || $_SESSION['user']->role != 1) {
    header('Location:../front/index.php');
} else {

$roles = GetAllRole();
$utilisateurs = GetAllUser();
$roleUtilisateur = GetRoleFromId(1);

?>

<div class="container">
    <h1>Liste des utilisateurs 
   <a href="signup.php"><button style="margin-left:49%;" title="Ajouter un utilisateur" type="button" class="btn btn-success"> + </button></a> </h1></h1>

    <table class="table table-striped table-sm">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Nom</th>
                <th scope="col">Prénom</th>
                <th scope="col">Pseudo</th>
                <th scope="col">E-Mail</th>
                <th scope="col">Role</th>
                <th scope="col">Changer rôle</th>
                <th scope="col">Supprimer utilisateur</th>
            </tr>
        </thead>
        <?php
        foreach ($utilisateurs as $utilisateur){
            $roleUtilisateur = GetRoleFromId($utilisateur['utilisateur_role']);
            $role = $roleUtilisateur['role_label'];
            ?>
            <tbody>
            <tr>
                <td><?php echo $utilisateur['utilisateur_id']; ?></td>
                <td><?php echo $utilisateur['utilisateur_nom']; ?></td>
                <td><?php echo $utilisateur['utilisateur_prenom']; ?></td>
                <td><?php echo $utilisateur['utilisateur_pseudo']; ?></td>
                <td><?php echo $utilisateur['utilisateur_mail']; ?></td>
                <td><?php echo $role ?></td>
                <td>
                    <?php
                    if ($utilisateur['utilisateur_role'] != 1){
                        ?>
                        <form class="col" action="../back/change_users_role.php" method="post">
                            <select name="role_id">
                                <?php foreach($roles as $r){
                                    ?>
                                    <option value="<?php echo($r[0]) ?>"><?php echo($r[1]) ?></option>
                                    <?php
                                } ?>
                            </select>
                            <p><input name="id" value="<?php echo $utilisateur['utilisateur_id'] ?>" type="hidden"></p>
                            <p><input type="submit" value="modifier"></p>
                        </form>
                        <?php
                    }
                    ?>
                </td>
                <td>
                    <a href="../back/delete_user.php?user_id=<?php echo $utilisateur['utilisateur_id']?>"><button type="button" class="btn btn-danger" title="Supprimer un utilisateur"> X </button></a>
                </td>
            </tr>
            </tbody>
            <?php
        }
        ?>
    </table>

</div>

<div>


</div>

<?php
}
?>