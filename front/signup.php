<?php
include_once("../include/header.php");
?>

<div class="container">
    <div class="col-xl-5 col-lg-6 col-md-8 col-sm-10" id="form">
        <h1>Inscription</h1>
        <form action="../back/signup_process.php" method="post">
            <div class="row">
                <div class="col">
                    <label class="form-label"for="surname">Nom :</label>
                    <input class="form-control" type="text" name="surname" id="nom" placeholder="nom" required />
                </div>

                <div class="col">
                    <label class="form-label" for="name">Prenom :</label>
                    <input class="form-control" type="text" name="name" id="name" placeholder="prenom" required />
                </div>
            </div>


            <div class="col">
                <label class="form-label" for="login">Pseudo :</label>
                <input class="form-control" type="text" name="login" id="login" placeholder="pseudo" required />
            </div>

            <div class="col">
                <label class="form-label" for="email">Email :</label>
                <input class="form-control" type="email" name="email" id="email" placeholder="email" required />
            </div>

            <div class="col">
                <label class="form-label" for="password_1">Mot de passe :</label>
                <input class="form-control" type="password" name="password_1" id="password_1" placeholder="*****" required />
            </div>

            <div class="col">
                <label class="form-label" for="password_2">Confirmer le mot de passe :</label>
                <input class="form-control" type="password" name="password_2" id="password_2" placeholder="*****" required />
            </div>

            <input class="btn btn-primary col-4" type="submit" value="S'inscrire" />
        </form>
        <a href="../front/connect.php">Se connecter</a>
    </div>
</div>

<?php
include_once("../include/footer.php");
?>