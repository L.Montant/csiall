<?php
include_once("../include/header.php");
include('../function/article_function.php');
include('../function/categorie_function.php');
include('../function/commentaire_function.php');
include('../function/utilisateur_function.php');

if (isset($_GET['article_id'])) {
    $article = GetArticleById($_GET['article_id']);
    $commentaires = GetAllCommentaireForOneArticle($_GET['article_id']);
}

if ($article != null) {
    ?>
    <hr>
    <div style="display:flex; padding:15px; text-align:center; margin-left:40px;">
        <div>
            <img src='<?php echo($article->image) ?>'>
        </div>
        <div style="margin-left:15px;">
            <div style="text-decoration: underline;"> <h1> Titre : <?php echo($article->titre) ?></h1></div>
            <div> <p><?php echo($article->contenu) ?> </p></div>
        </div>
    </div>
    <p style="margin-left:70px;"> C'est article a été écrit par : <?php echo(getUserNameById($article->id_utilisateur)[0]) ?></p>
    <hr>
    <?php
    $categories = getAllCategorieForOneArticle($article->id);

    if (isset($_SESSION['user'])) {

        $categorie = GetAllCategorie();

        if ($article->id_utilisateur == $_SESSION['user']->id) {
?>
<hr>
            <form action="../back/add_cat.php" method="post">
                <input type="hidden" name="article_id" value="<?php echo $_GET['article_id'] ?>">
                <select class="form-control" name="categorie_id" style="width: 200px;">
                    <?php foreach ($categorie as $cat) {
                    ?>
                        <option value="<?php echo ($cat[0]) ?>"><?php echo ($cat[1]) ?></option>
                    <?php
                    } ?>
                </select>
                <button class="btn btn-outline-primary" type="submit"> Ajouter catégorie a mon article </button>
            </form>
            <?php
        }
    }
    ?>
    <div style="display: flex;"> 
    <?php
    foreach ($categories as $cat) {
        $catName = GetCategorieNameFromId($cat[1]);
        if (isset($_SESSION['user'])) {
            if ($article->id_utilisateur == $_SESSION['user']->id) {
            ?>
                <form action="../back/delete_categorie.php" method="post">
                    <input type="hidden" name="article_id" value="<?php echo $_GET['article_id'] ?>">
                    <input type="hidden" name="categorie_id" value="<?php echo $cat[1] ?>">
                    <button type="submit" class="btn btn-outline-danger" style="margin-right:5px;" title="Supprimer la catégorie"> <?php echo($catName[0]) ?> X </button>
                </form>
    <?php
            }
        }
    }
    ?>
    </div>
<hr>
    <?php
    if (isset($_SESSION['user'])) {

    ?>

        <h5>Ecrire un commentaire : </h5>

        <div class="form-group" style="width:30%;">
            <form action="../back/add_com.php" method="post">
                <div>
                    <input type="text" class="form-control" name="titre" placeholder="titre du commentaire : ">
                    <input type="hidden" value="<?php echo $_GET['article_id'] ?>" name="articleid">
                    <textarea name="commentaire" class="form-control"></textarea>
                </div>
                <button class="btn btn-outline-primary" style="float:right;" >Ajouter mon commentaire</button>
            </form>
        </div>

    <?php
    }
    ?>
<br>
<br>
<hr>
    <h3 style="text-align: center;">Liste des commentaires</h3>
    <?php
    foreach ($commentaires as $commentaire) {
        $commentaireUser = getUserById($commentaire[1]);

    ?>
        <div>
        <hr>
            <h5 style="text-align: center;"> <?php echo ($commentaire[3]) ?></h5>
            <hr>
            <div>
            <img style="width:40px; height:40px; border:solid 1px black; border-radius:15px;" src="<?php echo($commentaireUser[6]) ?>">
            
            <h8 style="text-decoration:underline;">Utilisateur : <?php echo($commentaireUser[3])  ?></h8>
            <p> <?php echo ($commentaire[4])  ?></p> 
            </div>
        </div>

    <?php
    }
    ?>
    <hr>
    Article en rapport : 
    <div style="display:flex; justify-content:space-around;">
    <?php 
        if(count($categories) > 0){
            $articleLink = GetArticleFromSamecategorie($categorie[0][0], $article->id);
            if($articleLink != null){
                foreach($articleLink as $artcl){
                    ?> <div style="border: 1px solid black; border-radius:8px; padding:5px; margin-bottom:5px;" ><a label="Aller voir un autre article" href="article.php?article_id=<?php echo($artcl->id)?>"> <?php echo($artcl->titre) ?> </a>  </div> <?php
                }
            }

        } else {
            ?> Aucun article dans la meme catégorie <?php
        }

    ?>
    </div>
<?php
}
include_once("../include/footer.php");
?>