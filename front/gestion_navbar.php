<?php
include('../function/navbar_function.php');
include('../include/header.php');

?>

<form action="../back/gestion_navbar.php" method="post">
    <div style="display: flex;">
        <label for="position">Accueil : Position</label>
        <input name="accueil_bloc_num" type="number" max="4" min="1">

    </div>
    <div style="display: flex;">
        <label for="position">Nouvel article : Position</label>
        <input type="number" name="new_article_bloc_num" max="4" min="1">

    </div>
    <div style="display: flex; ">
        <label for="position">Mes articles : Position</label>
        <input type="number" name="my_article_bloc_num" max="4" min="1">

    </div>
    <div style="display: flex;">
        <label for="position">Nouvelle catégorie : Position</label>
        <input type="number" name="new_cat_bloc_num" max="4" min="1">
    </div>
    <button type="submit">Valider</button>
</form>