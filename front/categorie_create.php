<?php
include_once("../include/header.php");
include('../function/categorie_function.php');

if (!isset($_SESSION['user']) || $_SESSION['user']->role == 3) {
    header('Location:../front/index.php');
} else {

$allcat = GetAllCategorie();

?>

<div>
    <h1>Créer une nouvelle catégorie : </h1>
    <form action="../back/create_categorie.php" method="post">
        <div>
            <label class="form-label" for="titre">Nom de la catégorie :</label>
            <input class="w-50" class="form-control" type="titre" name="titre" id="titre" placeholder="Titre" required />
        </div>

        <input class="btn btn-primary" type="submit" value="Ajouter" />
    </form>

    <h1>Liste des catégories existantes :</h1>
    <table class="table">

        <?php
        foreach ($allcat as $cat) {

        ?> <tr>
                <th scope="col"> 
                <form  action="../back/update_cat.php" method="post">
                <input name="categorie_name" type="text" value="<?php echo ($cat[1]) ?>">
                </th>
                <th scope="col">

                        <input type="hidden" name="id_categorie" value="<?php echo($cat[0]) ?>">
                        <button type="submit" class="btn btn-primary">Modifier</button>
                    </form>
                </th>
                <th scope="col">
                <th scope="col">
                    <form  action="../back/del_cat.php" method="post">
                        <input type="hidden" name="id_categorie" value="<?php echo($cat[0]) ?>">
                        <button type="submit" class="btn btn-danger">Supprimer</button>
                    </form>
                </th>

                </th>

            <?php
        }
            ?>
            </tr>

    </table>
</div>


<?php
}
include_once("../include/footer.php");
?>