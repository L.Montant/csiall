<?php
include_once("../include/header.php");
include('../function/commentaire_function.php');
include('../function/utilisateur_function.php');

$commentaires = GetAllCommentaire();



if (!isset($_SESSION['user']) || $_SESSION['user']->role != 1) {
    header('Location:../front/index.php');
} else {


foreach ($commentaires as $commentaire) {
?>
    <hr>

    <form action="../back/update_com.php" method="post">
        <div class="row">
            <div class="col">
                <label> Nom de l'auteur du message : <b> <?php echo getUserNameById($commentaire[1])[0] ?> </b>   </label>
            </div>
            <div class="col">
                <input type="text" name="title" title="Titre du commentaire" class="form-control" value="<?php echo ($commentaire[3]) ?> ">
            </div>
            <div class="col">
                <input type="text" name="text" title="texte du commentaire" class="form-control" value="<?php echo ($commentaire[4]) ?> ">
            </div>
            <div class="col">
            <input type="hidden" name="id_com" value="<?php echo $commentaire[0] ?>">
                <button type="submmit" class="btn btn-warning">Modifier</button>

                <a href="../back/delete_commentaire.php?idcom=<?php echo $commentaire[0] ?>"><button type="button" class="btn btn-danger">Supprimer</button></a>
            </div>

        </div>
    </form>
<?php

}

}
include_once("../include/footer.php");
?>