<?php
include_once("../include/header.php");
?>

<div class="container">
    <div class="col-xl-5 col-lg-6 col-md-8 col-sm-10" id="form">
        <h1>Page de connexion</h1>
        <form action="../back/connect_process.php" method="post">
            <div class="col">
                <label class="form-label" for="login">Identifiant :</label>
                <input class="form-control" type="text" name="login" id="login" placeholder="identifiant" required />
            </div>
            <div class="col">
                <label class="form-label" for="password">Mot de passe :</label>
                <input class="form-control" type="password" name="password" id="password" placeholder="mot de passe" required />
            </div>
            <input class="btn btn-primary" type="submit" value="Se connecter" />
        </form>
        <a href="signup.php">S'inscrire</a>
    </div>
</div>

<?php
include_once("../include/footer.php");
?>