<?php 
include_once("../include/header.php");
include('../function/utilisateur_function.php');
include('../function/article_function.php');

if(isset($_SESSION['user'])){
    $user = $_SESSION['user'];
}else{
    header('Location:../front/index.php');
}

?>
<div class="container">
<form action="../back/updateaccount.php" method="post">
    <div>
        <label> Nom </label>
        <input type="text" id="nom" name="nom" class="form-control" value="<?php echo $user->nom ?>" >
    </div>
    <div>
        <label> Prénom </label>
        <input type="text" id="prenom" class="form-control" name="prenom" value="<?php echo $user->prenom ?>" >
    </div>
    <div>
        <label> Pseudo </label>
        <input type="text" id="pseudo" class="form-control" name="pseudo" value="<?php echo $user->pseudo ?>" >
    </div>
    <div>
        <label> Mail </label>
        <input type="text" id="mail" class="form-control" name="mail" value="<?php echo $user->mail ?>" >
    </div>
    <div>
        <label> Avatar </label>
        <input type="text" id="avatar"class="form-control" name="avatar" value="<?php echo $user->avatar ?>" >
    </div>
    <button type="submit" class="btn btn-primary"> Valider </button>
</form>
</div>
<?php
include_once("../include/footer.php");
?>