<?php
include_once("../include/header.php");
include('../function/article_function.php');

$AllArticle = GetAllArticle();

?>
<div class="album">
<div class="container">
    <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 row-cols-md-4 g-3">

<?php

foreach($AllArticle as $art){

    ?>
    <div class="col">
    <div class="card" style="width: 100%;">

        <img src="<?php echo $art[1] ?>" class="card-img-top" alt="...">
        <div class="card-body">
            <h5 class="card-title"><?php echo $art[2] ?></h5>
            <a href="article.php?article_id=<?php echo $art[0]?>" class="btn btn-primary">Voir l'article</a>
        </div>
    </div>
    </div>
    <?php
}

?>
    </div>
</div>
</div>
<?php

include_once("../include/footer.php");
?>
