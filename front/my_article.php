<?php
include_once("../include/header.php");
include('../function/article_function.php');

if (!isset($_SESSION['user']) || $_SESSION['user']->role == 3) {
    header('Location:../front/index.php');
} else {
    $mesArticles = GetArticleFromUserId($_SESSION['user']->id);

?>
    <div style="display:flex; flex-wrap:wrap;   padding-left: 0;">
        <?php
        foreach ($mesArticles as $article) {  ?>

            <div class="card" style="flex: 0 0 20%;margin-right: 5%; margin-left:5%; margin-top:5%;">
                <img class="card-img-top w-5" style="height: 300px;" src="<?php echo $article[1] ?>" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title"><?php echo $article[2] ?> <a style="float:right;" href="article.php?article_id=<?php echo $article[0] ?>" class="btn btn-primary">Voir l'article</a></h5>
                    
                </div>
                <a href="update_article.php?article_id=<?php echo $article[0] ?>" class="btn btn-primary">Mettre a jour l'article</a>
                <a href="../back/delete_article.php?article_id=<?php echo $article[0] ?>" class="btn btn-danger">Supprimer l'article</a>
            </div>
        <?php
        }
        ?>
    </div>


<?php
}
include_once("../include/footer.php");
?>
