<?php 
include_once("../include/header.php");
include('../function/article_function.php');

if (!isset($_SESSION['user']) || $_SESSION['user']->role == 3) {
    header('Location:../front/index.php');
} else {

?>

<div class="container">
    <div class="col-xl-5 col-lg-6 col-md-8 col-sm-10" id="form">
    <h1>Créer un nouvel article</h1>
        <form action="../back/create_article.php" method="post">
            <div class="col">
                <label class="form-label" for="titre">Titre  :</label>
                <input class="form-control" type="titre" name="titre" id="titre" placeholder="Titre" required />
            </div>
            <div class="col">
                <label class="form-label" for="contenu">Contenu :</label>
                <textarea class="form-control" rows="5" type="contenu" name="contenu" id="contenu" placeholder="Contenu" required></textarea>
            </div>
            <div class="col">
                <label class="form-label" for="image">Image :</label>
                <input class="form-control" type="text" name="image" id="image" placeholder="Lien de l'image" required />
            </div>
            <input class="btn btn-primary" type="submit" value="Ajouter" />
        </form>
    </div>
</div>


<?php
}
include_once("../include/footer.php");
?>
