<?php

use Utilisateur as GlobalUtilisateur;

Class Utilisateur{

    public $id;
    public $nom;
    public $prenom;
    public $pseudo;
    public $mail;
    public $mot_de_passe;
    public $avatar;
    public $role;

    public function __construct($id,$nom,$prenom,$pseudo,$mail,$mot_de_passe,$avatar,$role) // Constructeur 
    {
        $this->id = $id;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->pseudo = $pseudo;
        $this->mail = $mail;
        $this->mot_de_passe = $mot_de_passe;
        $this->avatar = $avatar;
        $this->role = $role;
    }

}

?>