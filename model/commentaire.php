<?php

Class Commentaire{

    public $id;
    public $id_utilisateur;
    public $titre;
    public $text;
    public $id_article;

    public function __construct($id,$id_utilisateur,$titre,$text,$id_article) // Constructeur 
    {
        $this->id = $id;
        $this->id_utilisateur = $id_utilisateur;
        $this->titre = $titre;
        $this->text = $text;
        $this->id_article = $id_article;
    }
}

?>