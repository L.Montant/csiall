<?php

Class Article{

    public $id;
    public $titre;
    public $contenu;
    public $image;
    public $id_utilisateur;

    public function __construct($id,$titre,$contenu,$image,$id_utilisateur) // Constructeur 
    {
        $this->id = $id;
        $this->titre = $titre;
        $this->contenu = $contenu;
        $this->image = $image;
        $this->id_utilisateur = $id_utilisateur;
    }
}

?>