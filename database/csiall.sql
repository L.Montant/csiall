-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : jeu. 01 juil. 2021 à 23:09
-- Version du serveur :  5.7.31
-- Version de PHP : 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `csiall`
--

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

DROP TABLE IF EXISTS `article`;
CREATE TABLE IF NOT EXISTS `article` (
  `article_id` int(11) NOT NULL AUTO_INCREMENT,
  `article_image` varchar(255) NOT NULL,
  `article_titre` varchar(255) NOT NULL,
  `article_contenu` varchar(255) NOT NULL,
  `id_utilisateur` int(11) NOT NULL,
  PRIMARY KEY (`article_id`),
  KEY `fk_article_utilisateur` (`id_utilisateur`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `article`
--

INSERT INTO `article` (`article_id`, `article_image`, `article_titre`, `article_contenu`, `id_utilisateur`) VALUES
(3, 'https://picsum.photos/200/300', 'testps', 'fkeozpfj', 8),
(4, 'https://picsum.photos/200/300', 'test', 'dazdzad zadza dazdz', 9),
(5, 'https://picsum.photos/200/300', 'dzadzad azdaz ', 'zdaz dadaz daz dzada zdaz daz', 9),
(6, 'https://picsum.photos/200/300', 'zadaz ', 'd adaz dazd az z', 9),
(7, 'https://picsum.photos/200/300', 'daz daez', 'za dzada zd zada& ', 9),
(8, 'https://picsum.photos/200/300', 'zerdae za', 'dza daz', 9),
(10, 'test2', 'test2', 'test2', 9);

-- --------------------------------------------------------

--
-- Structure de la table `article_categorie`
--

DROP TABLE IF EXISTS `article_categorie`;
CREATE TABLE IF NOT EXISTS `article_categorie` (
  `id_article` int(11) NOT NULL,
  `id_categorie` int(11) NOT NULL,
  KEY `fk_ac_article` (`id_article`),
  KEY `fk_ac_categorie` (`id_categorie`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `article_categorie`
--

INSERT INTO `article_categorie` (`id_article`, `id_categorie`) VALUES
(3, 4),
(3, 2),
(3, 5),
(10, 5),
(10, 8),
(4, 2),
(5, 2),
(6, 2),
(6, 4);

-- --------------------------------------------------------

--
-- Structure de la table `bloc`
--

DROP TABLE IF EXISTS `bloc`;
CREATE TABLE IF NOT EXISTS `bloc` (
  `bloc_id` int(11) NOT NULL AUTO_INCREMENT,
  `bloc_label` varchar(255) NOT NULL,
  `bloc_url` varchar(255) NOT NULL,
  `bloc_num` int(11) NOT NULL,
  `bloc_is_group` tinyint(1) DEFAULT NULL,
  `group_main_bloc_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`bloc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `bloc`
--

INSERT INTO `bloc` (`bloc_id`, `bloc_label`, `bloc_url`, `bloc_num`, `bloc_is_group`, `group_main_bloc_id`) VALUES
(1, 'Accueil', '/index.php', 3, 0, NULL),
(2, 'Nouvelle categorie', '/categorie_create.php', 4, 0, NULL),
(3, 'Nouvel article', '/article_create.php', 1, 0, NULL),
(4, 'Mes articles', '/my_article.php', 2, 0, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
CREATE TABLE IF NOT EXISTS `categorie` (
  `categorie_id` int(11) NOT NULL AUTO_INCREMENT,
  `categorie_label` varchar(255) NOT NULL,
  PRIMARY KEY (`categorie_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`categorie_id`, `categorie_label`) VALUES
(2, 'cat_2'),
(4, 'eza'),
(5, 'dzdz'),
(7, 'zeaaze'),
(8, 'daze'),
(9, 'LUCAS');

-- --------------------------------------------------------

--
-- Structure de la table `commentaire`
--

DROP TABLE IF EXISTS `commentaire`;
CREATE TABLE IF NOT EXISTS `commentaire` (
  `commentaire_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_utilisateur` int(11) NOT NULL,
  `id_article` int(11) NOT NULL,
  `commentaire_titre` varchar(255) NOT NULL,
  `commentaire_text` varchar(255) NOT NULL,
  PRIMARY KEY (`commentaire_id`),
  KEY `fk_commentaire_article` (`id_article`),
  KEY `fk_commentaire_utilisateur` (`id_utilisateur`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `commentaire`
--

INSERT INTO `commentaire` (`commentaire_id`, `id_utilisateur`, `id_article`, `commentaire_titre`, `commentaire_text`) VALUES
(3, 9, 3, 'J\'aime beaucoup', 'Tres sympa'),
(5, 9, 4, 'Bonjour', 'Quel article incroyable\r\n'),
(6, 9, 4, 'azd', 'dza'),
(7, 9, 10, 'eazeza', 'ezazaea'),
(8, 9, 10, 'titre', 'contenue du commentaire contenue du commentaire contenue du commentaire contenue du commentaire contenue du commentaire ');

-- --------------------------------------------------------

--
-- Structure de la table `menu`
--

DROP TABLE IF EXISTS `menu`;
CREATE TABLE IF NOT EXISTS `menu` (
  `id_bloc` int(11) NOT NULL,
  `menu_position` int(11) NOT NULL,
  KEY `fk_ib_bloc` (`id_bloc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_label` varchar(255) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `role`
--

INSERT INTO `role` (`role_id`, `role_label`) VALUES
(1, 'Admin'),
(2, 'Editeur'),
(3, 'Abonne');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `utilisateur_id` int(11) NOT NULL AUTO_INCREMENT,
  `utilisateur_nom` varchar(20) NOT NULL,
  `utilisateur_prenom` varchar(20) NOT NULL,
  `utilisateur_pseudo` varchar(30) NOT NULL,
  `utilisateur_mail` varchar(50) NOT NULL,
  `utilisateur_mot_de_passe` text NOT NULL,
  `utilisateur_avatar` varchar(255) NOT NULL,
  `utilisateur_role` int(11) NOT NULL,
  PRIMARY KEY (`utilisateur_id`),
  KEY `fk_utilisateur_role` (`utilisateur_role`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`utilisateur_id`, `utilisateur_nom`, `utilisateur_prenom`, `utilisateur_pseudo`, `utilisateur_mail`, `utilisateur_mot_de_passe`, `utilisateur_avatar`, `utilisateur_role`) VALUES
(5, 'Montant', 'Lucas', 'Lmontant', 'lucas.montant@gmail.com', '71b479f9616d4bb05740ac56174b9f419b41397c', 'https://lesexpertsdurecouvrement.com/wp-content/uploads/2015/11/default-avatar.jpg', 2),
(6, 'aze', 'aze', 'aze', 'aze@ze.fr', 'de271790913ea81742b7d31a70d85f50a3d3e5ae', 'https://www.google.com/url?sa=i&url=https%3A%2F%2Flesexpertsdurecouvrement.com%2Fexperts%2Fjacques-gelpi%2Fdefault-avatar%2F&psig=AOvVaw1ce8Y_pmaYcNIuUITNJDTG&ust=1621405365285000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCKia1unL0vACFQAAAAAdAAAAABAD', 2),
(7, 'aze', 'aze', 'aze', 'aze@ze.fr', 'de271790913ea81742b7d31a70d85f50a3d3e5ae', 'https://www.google.com/url?sa=i&url=https%3A%2F%2Flesexpertsdurecouvrement.com%2Fexperts%2Fjacques-gelpi%2Fdefault-avatar%2F&psig=AOvVaw1ce8Y_pmaYcNIuUITNJDTG&ust=1621405365285000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCKia1unL0vACFQAAAAAdAAAAABAD', 1),
(8, 'admin', 'admin', 'admin', 'admin@admin.fr', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'https://www.google.com/url?sa=i&url=https%3A%2F%2Flesexpertsdurecouvrement.com%2Fexperts%2Fjacques-gelpi%2Fdefault-avatar%2F&psig=AOvVaw1ce8Y_pmaYcNIuUITNJDTG&ust=1621405365285000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCKia1unL0vACFQAAAAAdAAAAABAD', 1),
(9, 'M', 'L', 'ml', 'ml@ml.fr', 'b331fc67af2f8f62216dc7677e33fa9ad545b96d', 'https://picsum.photos/200', 2);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `fk_article_utilisateur` FOREIGN KEY (`id_utilisateur`) REFERENCES `utilisateur` (`utilisateur_id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `article_categorie`
--
ALTER TABLE `article_categorie`
  ADD CONSTRAINT `fk_ac_article` FOREIGN KEY (`id_article`) REFERENCES `article` (`article_id`),
  ADD CONSTRAINT `fk_ac_categorie` FOREIGN KEY (`id_categorie`) REFERENCES `categorie` (`categorie_id`);

--
-- Contraintes pour la table `commentaire`
--
ALTER TABLE `commentaire`
  ADD CONSTRAINT `fk_commentaire_article` FOREIGN KEY (`id_article`) REFERENCES `article` (`article_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_commentaire_utilisateur` FOREIGN KEY (`id_utilisateur`) REFERENCES `utilisateur` (`utilisateur_id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `menu`
--
ALTER TABLE `menu`
  ADD CONSTRAINT `fk_ib_bloc` FOREIGN KEY (`id_bloc`) REFERENCES `bloc` (`bloc_id`);

--
-- Contraintes pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD CONSTRAINT `fk_utilisateur_role` FOREIGN KEY (`utilisateur_role`) REFERENCES `role` (`role_id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
