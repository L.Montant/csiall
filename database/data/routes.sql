-- bloc de base 

INSERT INTO `bloc`(`bloc_id`, `bloc_label`, `bloc_url`, `bloc_num`, `bloc_is_group`, `group_main_bloc_id`) VALUES (1,'accueil','/index.php',1,false,null);
INSERT INTO `bloc`(`bloc_id`, `bloc_label`, `bloc_url`, `bloc_num`, `bloc_is_group`, `group_main_bloc_id`) VALUES (2,'Nouvelle categorie','/categorie_create.php',2,false,null);
INSERT INTO `bloc`(`bloc_id`, `bloc_label`, `bloc_url`, `bloc_num`, `bloc_is_group`, `group_main_bloc_id`) VALUES (3,'Nouvel article','/article_create.php',3,false,null);
INSERT INTO `bloc`(`bloc_id`, `bloc_label`, `bloc_url`, `bloc_num`, `bloc_is_group`, `group_main_bloc_id`) VALUES (4,'Mes articles','/my_article.php',4,false,null);

-- bloc avec groupe 

UPDATE `bloc` SET `bloc_is_group`=true,`group_main_bloc_id`=4 WHERE `bloc_id` = 4;
UPDATE `bloc` SET `bloc_is_group`=true,`group_main_bloc_id`=4 WHERE `bloc_id` = 3;
UPDATE `bloc` SET `bloc_is_group`=true,`group_main_bloc_id`=4 WHERE `bloc_id` = 2;

-- bloc de base

UPDATE `bloc` SET `bloc_is_group`=false,`group_main_bloc_id`=null WHERE `bloc_id` = 4;
UPDATE `bloc` SET `bloc_is_group`=false,`group_main_bloc_id`=null WHERE `bloc_id` = 3;
UPDATE `bloc` SET `bloc_is_group`=false,`group_main_bloc_id`=null WHERE `bloc_id` = 2;


--- 