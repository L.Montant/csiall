<?php
require('../function/article_function.php');
require('../model/article.php');
require('../model/utilisateur.php');
session_start();

if(isset($_SESSION['user'])){
    if(isset($_GET['article_id'])){

        $article = GetArticleById($_GET['article_id']);

        if($article->id_utilisateur == $_SESSION['user']->id){
            DeleteArticle($_GET['article_id']);
        }
    }
}
header('Location: ' . $_SERVER['HTTP_REFERER']); 

