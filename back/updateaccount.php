<?php
    require('../function/utilisateur_function.php');
    require('../model/utilisateur.php');
    session_start();
    

    if(isset($_SESSION['user'])){
        $user = $_SESSION['user'];

        if(isset($_POST['nom']) && isset($_POST['prenom']) && isset($_POST['pseudo']) && isset($_POST['mail']) && isset($_POST['avatar'])){
            $user->nom = $_POST['nom'];
            $user->prenom = $_POST['prenom'];
            $user->pseudo = $_POST['pseudo'];
            $user->mail = $_POST['mail'];
            $user->avatar = $_POST['avatar'];
        }
        UpdateUser($user);
    }

    
    header('Location:../front/compte.php');
?> 