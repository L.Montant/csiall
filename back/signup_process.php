<?php
//connexion bdd
include_once("../include/database.php");
require('../function/utilisateur_function.php');
require('../model/utilisateur.php');

if(isset($_POST['surname']) && isset($_POST['name']) && isset($_POST['login']) && isset($_POST['email']) && isset($_POST['password_1']) && isset($_POST['password_2'])){

    if ($_POST['password_1'] != $_POST['password_2']){
        header("Location:../front/signup.php");
    }else{
        //hashage mot de passe
        $password_hash = sha1($_POST['password_1']);
        CreateNewUser(new Utilisateur(0,$_POST['surname'],$_POST['name'],$_POST['login'],$_POST['email'],$password_hash,"" ,2));
        header('Location:../front/connect.php');
    }

}

