<?php
include('../function/utilisateur_function.php');
require('../model/utilisateur.php');
session_start();


if(isset($_SESSION['user'])){
    if($_SESSION['user']->role == 1){
        if(isset($_GET['user_id'])){
            deleteUserById($_GET['user_id']);
        }
        header('Location: ' . $_SERVER['HTTP_REFERER']);  
    }
}
?>