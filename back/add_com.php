<?php
require('../function/commentaire_function.php');
require("../model/utilisateur.php");
require('../model/commentaire.php');
session_start();

if(isset($_SESSION['user'])){

    if(isset($_POST['articleid']) && isset($_POST['commentaire'])  && isset($_POST['titre'])){
        $monCommentaire = new Commentaire(0,$_SESSION['user']->id,$_POST['titre'],$_POST['commentaire'],$_POST['articleid']);
        CreateNewCommentaire($monCommentaire);
    }
}

header('Location: ' . $_SERVER['HTTP_REFERER']); 

