<?php
require('../model/utilisateur.php');
require('../model/article.php');
require('../function/article_function.php');
session_start();


if(isset($_SESSION['user']) && isset($_POST['id_article']) && isset($_POST['titre']) && isset($_POST['contenu']) && isset($_POST['image'])){
    $monArticle = new Article($_POST['id_article'],$_POST['titre'],$_POST['contenu'],$_POST['image'],$_SESSION['utilisateur']->id);
    UpdateArticle($monArticle);
}
header('Location: ' . $_SERVER['HTTP_REFERER']); 