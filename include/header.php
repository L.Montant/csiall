<!doctype html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <title>Projet CSIA</title>
    <!-- <link href="../css/style.css" rel="stylesheet"> -->
    <!-- Bootstrap CSS -->

    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</head>
<?php
include_once("../function/main.php");
session_start();

?>

<body>

    <?php if (isset($_SESSION['user']) && $_SESSION['user']->role == 2) {
        include('../function/navbar_function.php');
        $Blocks = LoadAllBlock();
    ?>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="../front/index.php">CSIA-PRESS</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <ul class="navbar-nav mr-auto">
                <?php
                foreach ($Blocks as $block) {
                    if ($block[4] == 0) {
                ?>
                        <li class="nav-item active">
                            <a class="nav-link" href="../front<?php echo ($block[2]) ?>"><?php echo ($block[1]) ?> <span class="sr-only"></span></a>
                        </li>
                        <?php
                    } else {
                        if ($block[0] == $block[5]) {
                        ?>
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <?php echo ($block[1]) ?>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="../front<?php echo ($block[2]) ?>"> <?php echo ($block[1]) ?></a>
                                <?php
                                foreach ($Blocks as $block2) {
                                    if ($block2[5] != null && $block2[5] = $block[1]) {
                                ?>
                                        <a class="dropdown-item" href="../front<?php echo ($block2[2]) ?>"> <?php echo ($block2[1]) ?></a>
                                <?php
                                    }
                                }
                                ?>
                            </div>
                <?php
                        }
                    }
                }
                ?>
                <div style="display: flex; position: absolute; left: 1200px;">
                    <li class="nav-item">
                        <a class="nav-link" href="../front/compte.php">Mon compte</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../back/disconnect.php">Se déconnecter</a>
                    </li>
                </div>
            </ul>
        </nav>
    <?php
    } else {
    ?>
        <div>
            <nav class="navbar navbar-expand-lg mb-3">
                <a class="navbar-brand" href="../front/index.php"><strong>CSIA-Press</strong></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="../front/index.php">Accueil</a>
                        </li>
                        <?php
                        if (isset($_SESSION['user']) && $_SESSION['user']->role != 3) {
                        ?>
                            <li class="nav-item">
                                <a class="nav-link" href="../front/categorie_create.php">Nouvelle catégorie</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="../front/article_create.php">Nouvel article</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="../front/my_article.php">Mes articles</a>
                            </li>
                            <?php
                            if ($_SESSION['user']->role == 1) {
                            ?>
                                <li class="nav-item">
                                    <a class="nav-link" href="../front/role.php">Utilisateurs</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="../front/gestion_admin.php">Gestion des commentaires</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="../front/gestion_navbar.php">Gestion de la navbar</a>
                                </li>
                        <?php
                            }
                        }
                        ?>
                    </ul>
                </div>
                <div>
                    <ul class="nav navbar-nav navbar-right">
                        <?php
                        if (!isset($_SESSION['user'])) {
                        ?>
                            <li class="nav-item">
                                <a class="nav-link" href="../front/signup.php">S'inscrire</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="../front/connect.php">Se connecter</a>
                            </li>
                        <?php
                        } else {
                        ?>
                            <li class="nav-item">
                                <a class="nav-link" href="../front/compte.php">Mon compte</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="../back/disconnect.php">Se déconnecter</a>
                            </li>
                        <?php
                        }
                        ?>
                    </ul>
                </div>
            </nav>
        </div>
    <?php
    }
    ?>


</body>