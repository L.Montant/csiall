<?php

//Création d'une nouvelle catégorie 
//Parametre $NewCategorie (Objet de type catégorie qui sera ajouté en base)
function CreateNewCategorie(Categorie $NewCategorie){
    require  "../include/database.php";

    $cat = GetAllCategorie();
    $alreadyexist = false;

    //On vérifie que la catégorie n'existe pas déja
    foreach($cat as $categorie){
        if($categorie[1] == $NewCategorie->label){
            $alreadyexist = true;
        }
    }

    //Si elle n'existe pas on l'ajoute donc.
    if(!$alreadyexist){
        $addNewCategorie = $bdd->prepare('INSERT INTO `categorie`(`categorie_label`) VALUES (?)');
        $addNewCategorie->bindParam(1,$NewCategorie->label);
        $addNewCategorie->execute();
    }
}

//Récupere toutes les catégories existantes.
function GetAllCategorie(){
    require  "../include/database.php";

    $AllCategorie = $bdd->prepare('SELECT * FROM `categorie`');
    $AllCategorie->execute();
    $value = $AllCategorie->fetchAll();

    return $value;
}

//récupere le nom d'une catégorie par rapport a son id 
//parametre $categorieId : Id de la catégorie
function GetCategorieNameFromId($categorieId){
    require  "../include/database.php";

    $AllCategorie = $bdd->prepare('SELECT `categorie_label` FROM `categorie` WHERE `categorie_id` =  ?');
    $AllCategorie->bindParam(1,$categorieId);
    $AllCategorie->execute();
    $value = $AllCategorie->fetch();

    return $value;
}

function DeleteCategorie($categorieId){
    require  "../include/database.php";

    $DeleteCatArticle = $bdd->prepare('DELETE FROM `article_categorie` WHERE `id_categorie` =  ?');
    $DeleteCatArticle->bindParam(1,$categorieId);
    $DeleteCatArticle->execute();

    $DeleteCat = $bdd->prepare('DELETE FROM `categorie` WHERE `categorie_id` =  ?');
    $DeleteCat->bindParam(1,$categorieId);
    $DeleteCat->execute();
}

function UpdateCategorie($categorieId, $categorieName){
    require  "../include/database.php";

    $DeleteCat = $bdd->prepare('UPDATE `categorie` SET `categorie_label`= ? WHERE `categorie_id` =  ?');
    $DeleteCat->bindParam(1,$categorieName);
    $DeleteCat->bindParam(2,$categorieId);
    $DeleteCat->execute();
}
?>