<?php

// récupère tous les rôles existants
function GetAllRole()
{
    require "../include/database.php";

    $AllRole = $bdd->prepare('SELECT * FROM `role`');
    $AllRole->execute();
    $value = $AllRole->fetchAll();

    return $value;

}

// récupère tous les rôles existants
function GetRoleFromId($roleId)
{
    require "../include/database.php";

    $GetLabel = $bdd->prepare('SELECT role_label FROM `role` WHERE role_id = ?');
    $GetLabel->bindParam(1,$roleId);
    $GetLabel->execute();
    $value = $GetLabel->fetch();

    return $value;

}

?>