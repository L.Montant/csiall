<?php

//Créer un nouveau commentaire pour un article
function CreateNewCommentaire(Commentaire $commentaire){
    require  "../include/database.php";

    $AddCommentary = $bdd->prepare('INSERT INTO `commentaire`(`id_utilisateur`, `commentaire_titre`, `commentaire_text`,`id_article`) VALUES (?,?,?,?)');
    $AddCommentary->bindParam(1,$commentaire->id_utilisateur);
    $AddCommentary->bindParam(2,$commentaire->titre);
    $AddCommentary->bindParam(3,$commentaire->text);
    $AddCommentary->bindParam(4,$commentaire->id_article);
    $AddCommentary->execute();

    // $idCommentaire = $bdd->lastInsertId();
}

//Retourne tous les commentaires pour un article
function GetAllCommentaireForOneArticle($article_id){
    require  "../include/database.php";

    $commentaireFromArticle = $bdd->prepare('SELECT * FROM `commentaire` WHERE `id_article` =  ?');
    $commentaireFromArticle->bindParam(1,$article_id);
    $commentaireFromArticle->execute();
    $value = $commentaireFromArticle->fetchAll();

    return $value;
}

//retournes tous les commentaires 
function GetAllCommentaire(){
    require  "../include/database.php";

    $commentaireFromArticle = $bdd->prepare('SELECT * FROM `commentaire`');
    $commentaireFromArticle->bindParam(1,$article_id);
    $commentaireFromArticle->execute();
    $value = $commentaireFromArticle->fetchAll();

    return $value;
}

//supprimer un commentaire
function DeleteCommentaire($commentaireId){
    require  "../include/database.php";

    $DeleteCommentaire = $bdd->prepare('DELETE FROM `commentaire` WHERE `commentaire_id` =  ?');
    $DeleteCommentaire->bindParam(1,$commentaireId);
    $DeleteCommentaire->execute();
}

//modifier un commentaire
function UpdateCommentaire($commentaireId,$commentaireTitle ,$commentairetext){
    require  "../include/database.php";

    $UpdateCat = $bdd->prepare('UPDATE `commentaire` SET `commentaire_text`= ?, `commentaire_titre` = ? WHERE `commentaire_id` =  ?');
    $UpdateCat->bindParam(1,$commentairetext);
    $UpdateCat->bindParam(2,$commentaireTitle);
    $UpdateCat->bindParam(3,$commentaireId);
    $UpdateCat->execute();
}