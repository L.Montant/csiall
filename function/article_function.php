<?php

//fonction de création d'un nouvel article 
//Parametre : $NewArticle (l'article qui sera ajouté a la bdd)
function CreateNewArticle(Article $NewArticle){
    require  "../include/database.php";

    $addNewArticle = $bdd->prepare('INSERT INTO `article`(`article_image`, `article_titre`, `article_contenu`, `id_utilisateur`) VALUES (?,?,?,?)');
    $addNewArticle->bindParam(1,$NewArticle->image);
    $addNewArticle->bindParam(2,$NewArticle->titre);
    $addNewArticle->bindParam(3,$NewArticle->contenu);
    $addNewArticle->bindParam(4,$NewArticle->id_utilisateur);
    $addNewArticle->execute();
}

//Récupère la liste de tous les articles
function GetAllArticle(){
    require  "../include/database.php";

    $AllArticle = $bdd->prepare('SELECT * FROM `article`');
    $AllArticle->execute();
    $value = $AllArticle->fetchAll();
    return $value;
}

//Récupere un article par Id
//Parametre : $id (L'id de l'article)
function GetArticleById($id){
    require  "../include/database.php";

    $myArticle = $bdd->prepare('SELECT * FROM `article` WHERE article_id = ? ');
    $myArticle->bindParam(1,$id);
    $myArticle->execute();
    $value = $myArticle->fetch();

    if($value != false){
        $finalArticle = new Article($value[0],$value[2],$value[3],$value[1],$value[4]);
        return ($finalArticle);
    }else{
        header('Location:../front/index.php');
    }
}

//Récupere tous les articles par rapport a l'Id d'un utilisateur
// Parametre : $userId (Id de l'utilisateur ciblé)
function GetArticleFromUserId($userId){
    require  "../include/database.php";

    $AllArticleFromuserId = $bdd->prepare('SELECT * FROM `article` WHERE `id_utilisateur` = ?');
    $AllArticleFromuserId->bindParam(1,$userId);
    $AllArticleFromuserId->execute();
    $value = $AllArticleFromuserId->fetchAll();

    return $value;
}

//ajoute une catégorie a un article 
//Parametre : $articleId (id de l'article) $categorieId (Id de la catégorie)
function AddCategorieToArticle($articleId, $categorieId){
    require  "../include/database.php";

    if(IsThisCategorieAlreadyOnArticle($categorieId,$articleId) == 0){
        $AddCategorie = $bdd->prepare('INSERT INTO `article_categorie`(`id_article`, `id_categorie`) VALUES (?,?)');
        $AddCategorie->bindParam(1,$articleId);
        $AddCategorie->bindParam(2,$categorieId);
        $AddCategorie->execute();
    }
}


//Vérifie si l'article existe deja sur la catégorie
//Parametre : $articleId (id de l'article) $categorieId (Id de la catégorie)
function IsThisCategorieAlreadyOnArticle($categorieId,$articleId){
    require  "../include/database.php";

    $Exist = $bdd->prepare('SELECT COUNT(*) FROM `article_categorie` WHERE `id_categorie` =  ? AND `id_article` =  ?');
    $Exist->bindParam(1,$categorieId);
    $Exist->bindParam(2,$articleId);
    $Exist->execute();
    $value = $Exist->fetch();
    return $value[0];
}


//Récupere toutes les catégories renseignée sur l'article
//Parametre : $articleId (id de l'article) 
function getAllCategorieForOneArticle($articleId){
    require  "../include/database.php";

    $AllCategorieFromArticleId = $bdd->prepare('SELECT * FROM `article_categorie` WHERE `id_article` = ?');
    $AllCategorieFromArticleId->bindParam(1,$articleId);
    $AllCategorieFromArticleId->execute();
    $value = $AllCategorieFromArticleId->fetchAll();

    return $value;
}

function DeleteCategorieFromArticle($articleId, $categorieId){
    require  "../include/database.php";

    $DeleteCat = $bdd->prepare('DELETE FROM `article_categorie` WHERE `id_categorie` =  ? AND `id_article` =  ?');


    $DeleteCat->bindParam(1,$categorieId);
    $DeleteCat->bindParam(2,$articleId);


    $DeleteCat->execute();
}

function UpdateArticle(Article $article){
    require  "../include/database.php";

    $UpdateArticle = $bdd->prepare('UPDATE `article` SET `article_image`= ?,`article_titre`= ?,`article_contenu`= ? WHERE `article_id`= ?');
    $UpdateArticle->bindParam(1,$article->image);
    $UpdateArticle->bindParam(2,$article->titre);
    $UpdateArticle->bindParam(3,$article->contenu);
    $UpdateArticle->bindParam(4,$article->id);
    $UpdateArticle->execute();
}

function DeleteArticle($articleId){
    require  "../include/database.php";

    $DeleteArticle = $bdd->prepare('DELETE FROM article WHERE article_id = ?');
    $DeleteArticle->bindParam(1,$articleId);
    $DeleteArticle->execute();
}

function GetArticleFromSamecategorie($categorieId, $articleId)
{
    require  "../include/database.php";

    $AllCategorieFromArticleId = $bdd->prepare('SELECT * FROM `article_categorie` WHERE `id_categorie` = ? && id_article != ? LIMIT 3');
    $AllCategorieFromArticleId->bindParam(1,$categorieId);
    $AllCategorieFromArticleId->bindParam(2,$articleId);
    $AllCategorieFromArticleId->execute();
    $value = $AllCategorieFromArticleId->fetchAll();

    $tab = [];
    foreach($value as $articleId){
        array_push($tab, GetArticleById($articleId[0])); 
    }
     return $tab;
}



?>