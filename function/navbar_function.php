<?php

function LoadAllBlock(){
    require  "../include/database.php";

    $LoadAllBlock = $bdd->prepare('SELECT * FROM `bloc` ORDER BY `bloc_num` ASC');
    $LoadAllBlock->execute();
    $Blocks = $LoadAllBlock->fetchAll();

    return $Blocks;
}

function UpdateBlock($position,$id_bloc){
    require  "../include/database.php";

    $updateBlock = $bdd->prepare('UPDATE `bloc` SET `bloc_is_group`=false,`group_main_bloc_id`=null,`bloc_num`=? WHERE `bloc_id` = ?');
    $updateBlock->bindParam(1,$position);
    $updateBlock->bindParam(2,$id_bloc);
    $updateBlock->execute();

}

function getIdofBlockByPosition($position){
    require  "../include/database.php";

    $LoadAllBlock = $bdd->prepare('SELECT `bloc_id` FROM `bloc` WHERE `bloc_num` = ?');
    $LoadAllBlock->bindParam(1,$position);
    $LoadAllBlock->execute();
    $Blocks = $LoadAllBlock->fetch();

    return $Blocks;
}