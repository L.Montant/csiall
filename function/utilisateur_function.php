<?php
//creer un nouvel utilisateur
//Un utilisateur
//void
function CreateNewUser(Utilisateur $user){
    require  "../include/database.php";

    $user->avatar = "https://www.google.com/url?sa=i&url=https%3A%2F%2Flesexpertsdurecouvrement.com%2Fexperts%2Fjacques-gelpi%2Fdefault-avatar%2F&psig=AOvVaw1ce8Y_pmaYcNIuUITNJDTG&ust=1621405365285000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCKia1unL0vACFQAAAAAdAAAAABAD";

    $addNewUser = $bdd->prepare('INSERT INTO `utilisateur`( `utilisateur_nom`, `utilisateur_prenom`, `utilisateur_pseudo`, `utilisateur_mail`, `utilisateur_mot_de_passe`, `utilisateur_avatar`,`utilisateur_role`) VALUES (?,?,?,?,?,?,?)');
    $addNewUser->bindParam(1,$user->nom);
    $addNewUser->bindParam(2,$user->prenom);
    $addNewUser->bindParam(3,$user->pseudo);
    $addNewUser->bindParam(4,$user->mail);
    $addNewUser->bindParam(5,$user->mot_de_passe);
    $addNewUser->bindParam(6,$user->avatar);
    $addNewUser->bindParam(7,$user->role);
    $addNewUser->execute();
}

//Connecte un nouvel utilisateur
//Login utilisateur et password 
// retourne l'utilisateur si les informations sont juste
function ConnectUser($userLogin, $UserPassword){
    require  "../include/database.php";

    $CheckPassword = $bdd->prepare("SELECT * FROM `utilisateur` WHERE utilisateur_pseudo = ? ");
    $CheckPassword->bindParam(1,$userLogin);
    $CheckPassword->execute();
    
    $UserData = $CheckPassword->fetch();

    if($UserData[5] == sha1($UserPassword)){
       $ConnUser = new Utilisateur($UserData[0],$UserData[1],$UserData[2],$UserData[3],$UserData[4],$UserData[5],$UserData[6],$UserData[7]);
       return $ConnUser;
    }
    return null;
}

function GetAllUser(){
    require  "../include/database.php";
    $GetUser = $bdd->prepare('SELECT * FROM `utilisateur`');
    $GetUser->execute();
    $Users = $GetUser->fetchAll();
    return $Users;
}

function UpdateUser(Utilisateur $user){
    require  "../include/database.php";
    $UpdateUser = $bdd->prepare('UPDATE `utilisateur` SET `utilisateur_nom`= ? ,`utilisateur_prenom`= ? ,`utilisateur_pseudo`= ? ,`utilisateur_mail`= ?, `utilisateur_avatar` = ? WHERE `utilisateur_id` = ?');
    $UpdateUser->bindParam(1,$user->nom);
    $UpdateUser->bindParam(2,$user->prenom);
    $UpdateUser->bindParam(3,$user->pseudo);
    $UpdateUser->bindParam(4,$user->mail);
    $UpdateUser->bindParam(5,$user->avatar);
    $UpdateUser->bindParam(6,$user->id);
    $UpdateUser->execute();
}

function UpdateUsersRole($user_id, $user_role){
    require  "../include/database.php";
    $UpdateUsersRole = $bdd->prepare('UPDATE `utilisateur` SET `utilisateur_role`= ? WHERE `utilisateur_id` = ?');
    $UpdateUsersRole->bindParam(1,$user_role);
    $UpdateUsersRole->bindParam(2,$user_id);
    $UpdateUsersRole->execute();
}

function getUserNameById($userId){
    require  "../include/database.php";

    $GetUser = $bdd->prepare('SELECT `utilisateur_pseudo` FROM `utilisateur`  WHERE `utilisateur_id` = ?');
    $GetUser->bindParam(1,$userId);
    $GetUser->execute();
    $userName = $GetUser->fetch();
    return $userName;
}

function deleteUserById($userId){
    require  "../include/database.php";

    
    $DelUser = $bdd->prepare('DELETE FROM `utilisateur` WHERE `utilisateur_id` = ?');
    $DelUser->bindParam(1,$userId);
    $DelUser->execute();
    
}


function getUserById($UserId){
    require  "../include/database.php";

    $GetUser = $bdd->prepare('SELECT * FROM `utilisateur`  WHERE `utilisateur_id` = ?');
    $GetUser->bindParam(1,$UserId);
    $GetUser->execute();
    $User = $GetUser->fetch();
    return $User;
}

// function getUserNameById($userId){

//     require  __DIR__ .'/bdd.php';
//     $GetUserName = $bdd->prepare('SELECT `prenom` FROM `utilisateur`  WHERE `id` = ?');
//     $GetUserName->bindParam(1,$userId);
//     $GetUserName->execute();
//     $Username = $GetUserName->fetch();
//     return $Username;
// }

?>